#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

#define MAX 4

// Pilha est�tica

struct Celula {
    int valor;
};

typedef struct pilha Pilha;

struct pilha {

    int qtd;
    struct Celula celulas[MAX];
};

Pilha *criar_pilha() {

    Pilha *pilha = malloc(sizeof(struct pilha));

    if (pilha != NULL) pilha->qtd = 0;

    return pilha;
}

void destruir_pilha(Pilha *pilha) {
    free(pilha);
}

int tamanho_pilha(Pilha *pilha) {
    return pilha == NULL ? -1 : pilha->qtd;
}

int pilha_cheia(Pilha *pilha) {

    return pilha == NULL ? -1 : pilha->qtd == MAX;
}

int pilha_vazia(Pilha *pilha) {

    return pilha == NULL ? -1 : pilha->qtd == 0;
}

int inserir_pilha(Pilha *pilha, struct Celula valor) {

    if (pilha == NULL || pilha->qtd == MAX) return 0;

    pilha->celulas[pilha->qtd] = valor;
    pilha->qtd++;

    return 1;
}

int remover_pilha(Pilha *pilha) {

    if (pilha == NULL || pilha->qtd == 0) return 0;

    pilha->qtd--;

    return 1;
}

void imprimir(Pilha *pilha) {

    if (pilha == NULL) return;

    int i;
    printf("\n");
    for (i = pilha->qtd - 1; i >= 0; i--)
        printf("%d\t", pilha->celulas[i].valor);
}

void informacoes_pilha(Pilha *pilha) {
    printf("\nPilha vazia: %d\tPilha cheia: %d\tTamanho pilha: %d", pilha_vazia(pilha), pilha_cheia(pilha), tamanho_pilha(pilha));
}

int main() {
    setlocale(LC_ALL, "Portuguese");
    printf("Hello, mund�o!\n");

    struct Celula celula[4] = {4, 3, 1, 2};
    Pilha *pilha = criar_pilha();

    printf("\nElementos no vetor Pilha.\n");
    for(int i = 0; i < 4; i++) printf("%d\t", celula[i].valor);

    informacoes_pilha(pilha);
    for (int i = 0; i < 4; i++) inserir_pilha(pilha, celula[i]);

    imprimir(pilha);

    informacoes_pilha(pilha);
    imprimir(pilha);

    for (int i = 0; i < 2; i++) remover_pilha(pilha);

    informacoes_pilha(pilha);
    imprimir(pilha);

    destruir_pilha(pilha);

    return 0;
}
